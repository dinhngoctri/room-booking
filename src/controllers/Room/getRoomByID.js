const RoomService = require("../../services/room.service");

const getRoomByID = () => {
  return async (req, res, next) => {
    try {
      const { roomId } = req.params;
      const data = await RoomService.getRoomByID(roomId);
      res.status(200).json({ data, message: "Get room success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getRoomByID;
