const RoomService = require("../../services/room.service");

const putUpdateRoom = () => {
  return async (req, res, next) => {
    try {
      let data = await RoomService.updateRoom(req);
      res.status(200).json({
        message: "update success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = putUpdateRoom;
