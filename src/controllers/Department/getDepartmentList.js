const DepartmentService = require("../../services/department.service");

const getDepartmentList = () => {
  return async (req, res, next) => {
    try {
      const data = await DepartmentService.getDepartmentList();
      res.status(200).json({ data, message: "Get department list success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getDepartmentList;
