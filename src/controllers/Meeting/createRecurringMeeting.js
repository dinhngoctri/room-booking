const MeetingService = require("../../services/meeting.service");

const createRecurringMeeting = () => {
  return async (req, res, next) => {
    try {
      req.body.userId = res.locals.user.id;
      const data = await MeetingService.createRecurringMeeting(req.body);
      res.status(200).json({ data, message: "Booking success" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = createRecurringMeeting;
