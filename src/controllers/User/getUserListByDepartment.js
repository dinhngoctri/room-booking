const UserService = require("../../services/user.service");

const getUserListByDepartmentController = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.getUserListByDepartment(req);
      res.status(200).json({
        message: "take a list of user success :)",
        data,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = getUserListByDepartmentController;
