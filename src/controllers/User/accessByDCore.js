const UserService = require("../../services/user.service");

const accessByDCore = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.accessByDCore(req.body);
      res.status(200).json({ data, message: "Access success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = accessByDCore;
