const UserService = require("../../services/user.service");

const takeAListOfUserDelelteController = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.takeAListOfUserDelelte(req);
      res.status(200).json({
        message: "take list user deleted success :)",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = takeAListOfUserDelelteController;
