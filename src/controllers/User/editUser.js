const UserService = require("../../services/user.service");

const PutUpdateUser = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      let data = await UserService.updateUser(id, req);
      res.status(200).json({
        message: "update success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = PutUpdateUser;
