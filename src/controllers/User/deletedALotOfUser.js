const UserService = require("../../services/user.service");

const deleteAlotOfUserController = () => {
  return async (req, res, next) => {
    try {
      const { arrUserId } = req.body;
      const result = await UserService.deleteAlotOfUser(arrUserId);
      res.status(200).json({
        message: "deleted success",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = deleteAlotOfUserController;
