const UserService = require("../../services/user.service");

const findUserByRefId = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.findUserByRefId(req.params.refId);
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = findUserByRefId;
