const UserService = require("../../services/user.service");

const register = () => {
  return async (req, res, next) => {
    try {
      await UserService.register(req.body);
      res.status(200).json({ message: "Register success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = register;
