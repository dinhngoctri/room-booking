const UserService = require("../../services/user.service");

const addUserInListController = () => {
  return async (req, res, next) => {
    try {
      let data = await UserService.addUserInList(req);
      res.status(200).json({
        message: "add user success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = addUserInListController;
