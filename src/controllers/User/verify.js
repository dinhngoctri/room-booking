const verify = () => {
  return async (req, res, next) => {
    try {
      res.status(200).json({ data: res.locals.user, message: "Verify success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = verify;
