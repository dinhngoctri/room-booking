const BookingInfoService = require("../../services/bookingInfo.service");

const getBookingList = () => {
  return async (req, res, next) => {
    try {
      req.body.role = res.locals.user.role;
      const data = await BookingInfoService.getBookingList(req);
      res.status(200).json({
        message: "take a list of booking info success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getBookingList;
