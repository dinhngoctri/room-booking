const BookingInfoService = require("../../services/bookingInfo.service");

const saveEmailToParticipantsController = () => {
  return async (req, res, next) => {
    try {
      const saveToDatabase = await BookingInfoService.saveEmailToParticipants(req);
      res.status(200).json({
        message: "save user in participants success",
        saveToDatabase,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = saveEmailToParticipantsController;
