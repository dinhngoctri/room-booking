const BookingInfoService = require("../../services/bookingInfo.service");

const searchBookingInfoForAdminController = () => {
  return async (req, res, next) => {
    try {
      const data = await BookingInfoService.searchBookingInfoForAdmin(req);
      res.status(200).json({
        message: "take a list of user success :)",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = searchBookingInfoForAdminController;
