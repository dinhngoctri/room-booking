const BookingInfoService = require("../../services/bookingInfo.service");

const getListOfMeetingsYouAttendController = () => {
  return async (req, res, next) => {
    try {
      const data = await BookingInfoService.listOfMeetingsYouAttend(req);
      res.status(200).json({
        message: "take a list of meeting you attend success ",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getListOfMeetingsYouAttendController;
