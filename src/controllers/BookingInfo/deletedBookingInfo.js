const BookingInfoService = require("../../services/bookingInfo.service");

const deletedBookingInfo = () => {
  return async (req, res, next) => {
    try {
      const id = req.params.id;
      const reason = req.body.reason;
      const data = await BookingInfoService.deletedBookingInfo(id, reason ,req);
      res.status(200).json({
        message: "deleted success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = deletedBookingInfo;
