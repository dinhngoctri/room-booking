const sendEmail = require("../../services/gmail.service");

const sendEmailBookinInfo = () => {
  return async (req, res, next) => {
    try {
      const result = await sendEmail.send(req);
      res.status(200).json({
        message: "send email success",
        result,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = sendEmailBookinInfo;
