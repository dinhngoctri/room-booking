const sendEmail = require("../../services/gmail.service");

const checkSendEmailSuccessController = () => {
  return async (req, res, next) => {
    try {
      const result = await sendEmail.checkSendEmailSuccess(req);
      res.status(200).json({
        message: "check send email finish",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = checkSendEmailSuccessController;
