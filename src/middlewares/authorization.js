// middleware verify token
const jwt = require("jsonwebtoken");
const { AppError } = require("../helpers/error");
const { User } = require("../models/index");

const extractTokenFromHeader = (headers) => {
  const bearerToken = headers.authorization;

  if (!bearerToken) {
    throw new AppError(400, "Token not found");
  }

  const parts = bearerToken.split(" ");
  if (parts.length !== 2 || parts[0] !== "Bearer" || !parts[1].trim()) {
    throw new AppError(401, "Invalid token");
  }
  return parts[1];
};

const authorization = async (req, res, next) => {
  try {
    const token = extractTokenFromHeader(req.headers);
    const payload = jwt.verify(token, "didongviet");
    const user = await User.findByPk(payload.id);

    if (!user) {
      throw new AppError(401, "Token không hợp lệ.");
    }

    if (user.deleted) {
      throw new AppError(403, "Tài khoản đã bị chặn hoặc bị khoá.");
    }

    if (user.role !== payload.role) {
      throw new AppError(
        401,
        "Quyền truy cập của tài khoản đã bị thay đổi, vui lòng đăng nhập lại."
      );
    }

    res.locals.user = user;
    next();
  } catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      next(new AppError(401, "Invalid token"));
    }
    next(error);
  }
};

const authorizationAdmin = async (req, res, next) => {
  try {
    const token = extractTokenFromHeader(req.headers);
    const payload = jwt.verify(token, "didongviet");
    const user = await User.findByPk(payload.id);

    if (!user) {
      throw new AppError(401, "Invalid token");
    }

    if (user.role !== "admin")
      throw new AppError(403, "403 Forbidden - bị từ chối không cho phép.");

    res.locals.user = payload;
    next();
  } catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      next(new AppError(401, "Invalid token"));
    }
    next(error);
  }
};

module.exports = { authorization, authorizationAdmin };
