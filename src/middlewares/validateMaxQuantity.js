const { AppError } = require("../helpers/error");
const { Participant } = require("../models/index");

const checkseats = async (req, res, next) => {
  try {
    const { bookingId, seats, emailList } = req.body;
    const arrUserUnderDatabase = await Participant.findAll({
      where: { bookingId, deleted: 0 },
      raw: true,
    });

    emailList.forEach((item) => {
      arrUserUnderDatabase.push(item);
    });

    if (!arrUserUnderDatabase || emailList.length > seats) {
      throw new AppError(400, "people exceed the maximum salary");
    }

    if (arrUserUnderDatabase.length > seats) {
      throw new AppError(400, "people exceed the maximum salary");
    }

    next();
  } catch (error) {
    next(error);
  }
};

module.exports = checkseats;
