"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Departments", {
      id: {
        autoIncrement: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      departmentName: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      departmentColor: {
        type: Sequelize.STRING(50),
        defaultValue: "#027bff",
      },
      telegramGroupId: {
        type: Sequelize.STRING,
      },
      deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("DEPARTMENTS");
  },
};
