"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Users", {
      id: {
        autoIncrement: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },

      refId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },

      fullname: {
        type: Sequelize.STRING,
      },

      email: {
        type: Sequelize.STRING(250),
        allowNull: false,
        unique: "email",
      },

      // password: {
      //   type: Sequelize.STRING(150),
      //   allowNull: false,
      // },

      departmentId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },

      role: {
        type: Sequelize.ENUM("user", "admin"),
        defaultValue: "user",
      },

      deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("USERS");
  },
};
