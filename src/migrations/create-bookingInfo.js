"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("BookingInfos", {
      id: {
        autoIncrement: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      startingTime: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      endingTime: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      departmentId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      meetingContent: {
        type: Sequelize.STRING(250),
      },
      status: {
        type: Sequelize.ENUM("cancel", "done", "comingUp"),
        defaultValue: "comingUp",
      },
      reason: {
        type: Sequelize.STRING(250),
        defaultValue: "",
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      roomId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("BOOKING_INFO");
  },
};
