const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      email: {
        type: DataTypes.STRING(250),
        allowNull: false,
      },
      refId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      fullname: {
        type: DataTypes.STRING,
      },
      role: {
        type: DataTypes.ENUM("user", "admin"),
        allowNull: true,
        defaultValue: "user",
      },
      departmentId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Departments",
          key: "id",
        },
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
      },
      // password: {
      //   type: DataTypes.STRING(150),
      //   allowNull: false,
      //   set(value) {
      //     const salt = bcrypt.genSaltSync();
      //     const hashedPassword = bcrypt.hashSync(value, salt);
      //     this.setDataValue("password", hashedPassword);
      //   },
      // },
    },
    {
      sequelize,
      tableName: "Users",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
        {
          name: "departmentID",
          using: "BTREE",
          fields: [{ name: "departmentId" }],
        },
      ],
    }
  );

  User.associate = function (models) {
    User.belongsTo(models.Department, {
      as: "department",
      foreignKey: "departmentId",
    });
    User.hasMany(models.BookingInfo, {
      as: "BookingInfos",
      foreignKey: "userId",
    });
    User.hasMany(models.Participant, {
      as: "Participants",
      foreignKey: "userId",
    });
  };
  return User;
};
