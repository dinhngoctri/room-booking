const { formatTime } = require("./formatTime");
const { replaceStringInHmtl } = require("./replaceStringInHtml");
const { Room, User } = require("../models/index");
const fs = require("fs");

const emailMessageContent = async (bookingDetailUnderData, req, status, subject) => {
  let { emailList, user, bookingId } = req;
  console.log("emailList: ", emailList);

  // if (Array.isArray(emailList) && typeof emailList[0] === "object") {
  //   emailList = emailList.map((item) => item.label);
  // }

  // let emailListUnderData = await User.findAll({
  //   where: {
  //     id: emailList,
  //   },
  //   attributes: ["email"],
  //   raw: true,
  // });

  // emailListUnderData = emailListUnderData.map((item) => item.email);
  // emailListUnderData =emailList

  // console.log("emailListUnderData", emailListUnderData);

  const { id, startingTime, endingTime, meetingContent, roomId } = bookingDetailUnderData;

  console.log("bookingDetailUnderData", bookingDetailUnderData);

  const room = await Room.findOne({
    where: { id: roomId },
    attributes: ["roomName"],
    raw: true,
  });

  const startingTimeMoment = formatTime(startingTime, "hh:mm");
  const endingTimeMoment = formatTime(endingTime, "hh:mm");
  const date = formatTime(startingTime, "DD/MM/YYYY");

  let html = fs.readFileSync("./views/pages/index.html", "utf8");

  html = replaceStringInHmtl(
    "{meetingContent}",
    html,
    meetingContent,
    "không có tiêu đề"
  );

  html = replaceStringInHmtl("{date}", html, date);

  html = replaceStringInHmtl("{startingTimeMoment}", html, startingTimeMoment);

  html = replaceStringInHmtl("{endingTimeMoment}", html, endingTimeMoment);

  html = replaceStringInHmtl("{roomName}", html, room.roomName);

  html = replaceStringInHmtl("{id}", html, bookingId, id);

  html = replaceStringInHmtl("{statusMeeting}", html, status);

  const options = {
    to: `${user}`,
    cc: emailList,
    subject: `DDV 🚀 - ${subject}`,
    text: "This email is sent from the DDV",
    html,
    textEncoding: "base64",
    headers: [
      { key: "X-Application-Developer", value: "Amit Agarwal" },
      { key: "X-Application-Version", value: "v1.0.0.2" },
    ],
  };

  return options;
};

module.exports = emailMessageContent;
