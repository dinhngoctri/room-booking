const moment = require("moment");

const telegramMessageContent = (meetingInfo) => {
  moment.locale("vi");
  let { participants, startingTime, endingTime, meetingContent, roomName, reason } =
    meetingInfo;
  participants = participants.map((user) => user.fullname).join(", ");
  const getDay = (date) => moment(date).format("dddd, DD/MM/YYYY");
  const getTime = (date) => moment(date).format("HH:mm");

  const newMeeting = () => {
    return `
    <b>Bạn có một lịch họp mới</b> 🗓 🗓 🗓 
    
<b>Vào</b> ${getDay(startingTime)},
<b>Lúc</b> ${getTime(startingTime)} và <b>kết thúc</b> vào lúc ${getTime(endingTime)}.
<b>Tại</b> ${roomName}
    
<b>Nội dung cuộc họp:</b> ${
      meetingContent ? meetingContent : "Không tìm thấy nội dung cuộc họp"
    }.
<b>Danh sách tham dự gồm:</b> ${
      participants ? participants : "Không tìm thấy danh sách người tham dự"
    }.
          `;
  };

  const cancelMeeting = () => {
    return `
    <pre>Lịch họp bị huỷ</pre> ❗️❗️❗️ 
    
Lịch họp của bạn,
<b>Ngày</b> ${getDay(startingTime)},
<b>Lúc</b> ${getTime(startingTime)}.
<b>Tại</b> ${roomName} <b>đã bị huỷ</b>.

<b>Lý do cuộc họp bị huỷ</b> ${reason}.
<b>Nội dung cuộc họp:</b> ${
      meetingContent ? meetingContent : "Không tìm thấy nội dung cuộc họp"
    }.
          `;
  };

  const remindMeeting = () => {
    return `
    <b>Bạn có một lịch họp ngày hôm nay</b> 🕖 🕖 🕖
    
<b>Lúc</b> ${getTime(startingTime)} và <b>kết thúc</b> vào lúc ${getTime(endingTime)}.
<b>Tại</b> ${roomName}
    
<b>Nội dung cuộc họp:</b> ${
      meetingContent ? meetingContent : "Không tìm thấy nội dung cuộc họp"
    }.
<b>Danh sách tham dự gồm:</b> ${
      participants ? participants : "Không tìm thấy danh sách người tham dự"
    }.
          `;
  };

  return { newMeeting, cancelMeeting, remindMeeting };
};

module.exports = telegramMessageContent;
