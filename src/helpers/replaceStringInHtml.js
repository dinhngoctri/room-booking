const replaceStringInHmtl = (content, html, dataBinding, dataBinding2) => {
  const reg = new RegExp(content, "g");
  if (html.match(reg)) {
    return html.replace(reg, dataBinding || dataBinding2);
  }
};

module.exports = { replaceStringInHmtl };
