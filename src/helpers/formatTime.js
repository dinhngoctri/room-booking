const moment = require("moment");

const formatTime = (time, typeFormat) => {
  return moment(time).format(typeFormat);
};

module.exports = { formatTime };
