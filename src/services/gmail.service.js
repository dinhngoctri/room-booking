const { google } = require("googleapis");
const MailComposer = require("nodemailer/lib/mail-composer");
const credentials = require("../config/credentials.json");
const tokens = require("../middlewares/token.json");
const moment = require("moment");
const { BookingInfo, Room, User, Participant } = require("../models/index");
const { AppError } = require("../helpers/error");
const { Op } = require("sequelize");
const cron = require("node-cron");
const fs = require("fs");
const { formatTime } = require("../helpers/formatTime");
const emailMessageContent = require("../helpers/emailMessaageContent");
const path = require("path");

const CUOC_HOP_BI_HUY = "Cuộc Họp Bị Hủy";
const HUY_CUOC_HOP = "Hủy Cuộc Họp";
const CUOC_HOP_SAP_DIEN_RA = "Cuộc Họp Sắp Diễn Ra";
const THU_MOI_HOP = "Thư Mời Họp";
const NHAC_LICH_HOP = "Nhắc Lịch Họp";

const REFRESH_TOKEN =
  "1//0eM_TNsRxh4NPCgYIARAAGA4SNwF-L9IrlIWoQgyKEmg3Vm4EHZZKpa--6Ox0ZI13WkjtZSmy_HSRV7lVq5PjlHKxqBDZicc-9EQ";

const ACCESS_TOKEN =
  "ya29.a0Ael9sCPen0z2ZUdclo6q1qUfO_3ZdDfhGFREKqSayr_k38o7uSX2pyBTZV2KgPtYuS9HVQxVQi2VEpMnfAaIg1k5SeCIKGIJGRDDUGUZpu8AvsHYKP0RzVmDjpc99T0tufAD1UVSzFdZQaHwurODA4ZVnB7FaCgYKAXMSARISFQF4udJhHEI1yZvK23-u39rNVCV5OA0163";

const sendEmail = {
  // Hàm thực hiện việc làm mới token
  async refreshAccessTokenIfNeeded(oAuth2Client) {
    const expiryDate = new Date(tokens.expiry_date);
    const currentDate = new Date();

    // Nếu access token đã hết hạn, sử dụng refresh token để lấy access token mới
    if (expiryDate < currentDate) {
      const dataFormGoogle = await oAuth2Client.refreshToken(REFRESH_TOKEN);

      let newTokens = {
        ...dataFormGoogle.tokens,
        refresh_token: REFRESH_TOKEN,
      };
      const tokenPath = path.join(__dirname, "../middlewares/token.json");

      fs.writeFileSync(tokenPath, JSON.stringify(newTokens));

      return oAuth2Client.setCredentials(newTokens);
    } else {
      return oAuth2Client.setCredentials(tokens);
    }
  },

  //* ----------------------------------------------------------
  async getGmailService() {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id,
      client_secret,
      redirect_uris[0]
    );
    await this.refreshAccessTokenIfNeeded(oAuth2Client);
    const gmail = google.gmail({ version: "v1", auth: oAuth2Client });
    return gmail;
  },

  encodeMessage(message) {
    return Buffer.from(message)
      .toString("base64")
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=+$/, "");
  },

  async createMail(options) {
    const mailComposer = new MailComposer(options);
    const message = await mailComposer.compile().build();
    return this.encodeMessage(message);
  },

  async sendMail(options) {
    const gmail = await this.getGmailService();
    const rawMessage = await this.createMail(options);
    const { data: { id } = {} } = await gmail.users.messages.send({
      userId: "me",
      resource: {
        raw: rawMessage,
      },
    });
    return id;
  },

  async send(req) {
    try {
      const { userArray, user, userIdLogin, bookingId } = req.body;

      let emailList = await User.findAll({
        where: { id: userArray },
        attributes: ["email"],
        raw: true,
      });
      emailList = emailList.map((user) => user.email);

      const checkEmailListIsEmpty = emailList.find((item) => item === "");

      if (checkEmailListIsEmpty === "")
        throw new AppError(400, "có người trong danh sách không có email");

      const bookingDetailUnderData = await BookingInfo.findOne({
        where: { id: bookingId },
        raw: true,
      });

      const { status, userId } = bookingDetailUnderData;

      if (status === "done")
        throw new AppError(400, "cuộc họp đã xong nên không cho gửi mail");

      // todo: cần làm lại ----> check admin theo token

      const checkAdmin = await User.findOne({
        where: {
          id: userIdLogin,
        },
      });

      //!------------------->  nếu cuộc họp có status === cancel

      if (status === "cancel") {
        const options = await emailMessageContent(
          bookingDetailUnderData,
          { emailList, user, userIdLogin, bookingId },
          CUOC_HOP_BI_HUY,
          HUY_CUOC_HOP
        );

        const messageId = await this.sendMail(options);

        return { messageId, emailList };
      }

      // !----------------------------------- admin -----------------

      if (checkAdmin.role === "admin") {
        const options = await emailMessageContent(
          bookingDetailUnderData,
          { emailList, user, userIdLogin, bookingId },
          CUOC_HOP_SAP_DIEN_RA,
          THU_MOI_HOP
        );

        const messageId = await sendEmail.sendMail(options);

        this.remindSendByEmail(req, bookingDetailUnderData);

        return { messageId, emailList };
      }

      // !---------------------- user --------------------------

      if (userId !== userIdLogin)
        throw new AppError(
          400,
          "bạn không phải là chủ cuộc họp , nên bạn không có quyền xóa"
        );

      const options = await emailMessageContent(
        bookingDetailUnderData,
        { emailList, user, userIdLogin, bookingId },
        CUOC_HOP_SAP_DIEN_RA,
        THU_MOI_HOP
      );

      const messageId = await sendEmail.sendMail(options);

      this.remindSendByEmail(req, bookingDetailUnderData);

      return { messageId, emailList };
    } catch (error) {
      console.log("error: ", 99999, error);
      throw error;
    }
  },

  async checkSendEmailSuccess(req) {
    try {
      const { userArray, bookingId } = req.body;
      return await Participant.update(
        { announced: 1 },
        {
          where: {
            userId: {
              [Op.in]: userArray,
            },
            bookingId,
          },
        }
      );
    } catch (error) {
      throw error;
    }
  },

  async remindSendByEmail(req, bookingDetailUnderData) {
    try {
      const { emailList, bookingId } = req.body;

      const { startingTime } = bookingDetailUnderData;

      const timeRemind = moment(startingTime)
        .subtract(1, "hour")
        .format("YY-MM-DD HH:mm:ss");

      const [years, time] = timeRemind.split(" ");
      const [year, month, date] = years.split("-");
      const [hours, minutes, seconds] = time.split(":");

      const remind = cron.schedule(
        ` ${minutes} ${hours} ${date} ${month} *`,
        async () => {
          const boookingDetailCurrent = await BookingInfo.findOne({
            where: { id: bookingId },
            raw: true,
          });

          const { status } = boookingDetailCurrent;

          if (status !== "comingUp")
            throw new AppError(400, "cuộc họp đã bị hủy hoặc đã xong");

          const options = await emailMessageContent(
            boookingDetailCurrent,
            req,
            CUOC_HOP_SAP_DIEN_RA,
            NHAC_LICH_HOP
          );

          const messageId = await this.sendMail(options);

          return { messageId, emailList };
        },
        {
          scheduled: true,
          timezone: "Asia/Ho_Chi_Minh",
        }
      );

      remind.start();
    } catch (error) {
      throw error;
    }
  },
};

module.exports = sendEmail;
