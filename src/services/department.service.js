const { AppError } = require("../helpers/error");
const { isNumber } = require("../helpers/validation");
const { Department } = require("../models/index");

const DepartmentService = {
  async getDepartmentList() {
    try {
      return Department.findAll({ where: { deleted: false } });
    } catch (error) {
      console.log("error: ", 9999, error);
      throw error;
    }
  },

  async getDepartmentByID(departmentId) {
    try {
      if (!isNumber(departmentId)) {
        throw new AppError(400, "'departmentId' must be a integer and required.");
      }
      const deparment = await Department.findByPk(departmentId);
      if (!deparment) {
        throw new AppError(404, "Invalid 'deparmentID'");
      }
      return deparment;
    } catch (error) {
      throw error;
    }
  },
};

module.exports = DepartmentService;
