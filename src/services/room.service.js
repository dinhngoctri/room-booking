const { AppError } = require("../helpers/error");
const { Room } = require("../models/index");

const RoomService = {
  async getRoomList() {
    try {
      return await Room.findAll({ where: { deleted: false } });
    } catch (error) {
      console.log("error: ", 1231, error);
      throw error;
    }
  },

  async getRoomByID(roomId) {
    try {
      const room = await Room.findByPk(roomId);
      if (!room) {
        throw new AppError(404, "Id phòng họp không tồn tại.");
      }
      if (room.deleted) {
        throw new AppError(
          403,
          "Phòng họp hiện tại đang bị khoá hoặc tạm ngưng sử dụng."
        );
      }

      return room;
    } catch (error) {
      throw error;
    }
  },
  async getAListOfRoom() {
    try {
      return await Room.findAll();
    } catch (error) {
      throw error;
    }
  },
  async updateRoom(req) {
    try {
      const { id } = req.params;
      const { roomName, seats } = req.body;
      if (!roomName) {
        throw new AppError(400, "roomName is not undifine");
      }

      if (!seats) {
        throw new AppError(400, " seats is not undifine");
      }

      const seatsParseint = parseInt(seats);

      const checkRoomID = await Room.findByPk(id);
      if (checkRoomID) {
        const update = {
          roomName,
          seats: seatsParseint,
        };
        const result = await Room.update(update, { where: { id } });
        return result;
      }
      return false;
    } catch (error) {
      throw error;
    }
  },

  async createRoom(req) {
    try {
      const { roomName, seats } = req.body;

      if (!roomName) {
        throw new AppError(400, "roomName is not undifine");
      }

      if (!seats) {
        throw new AppError(400, " seats is not undifine");
      }

      const seatsParseint = parseInt(seats);

      const newRoom = {
        roomName,
        seats: seatsParseint,
      };
      return await Room.create(newRoom);
    } catch (error) {
      throw error;
    }
  },

  async deletedRoom(id) {
    try {
      const checkRoomID = await Room.findByPk(id);
      if (checkRoomID) {
        const update = {
          deleted: true,
        };
        const result = await Room.update(update, { where: { id } });
        return result;
      }
      return false;
    } catch (error) {
      throw error;
    }
  },

  async restoreRoom(id) {
    try {
      const checkRoomID = await Room.findByPk(id);
      if (checkRoomID) {
        const update = {
          deleted: false,
        };
        const result = await Room.update(update, { where: { id } });
        return result;
      }
      return false;
    } catch (error) {
      throw error;
    }
  },
};

module.exports = RoomService;
