const axios = require("axios");
const { TELEGRAM_API } = require("../config/telegram");
const { BookingInfo, Room, Department } = require("../models/index");
const { getUserListInParticipant } = require("./meeting.service");
const telegramMessageContent = require("../helpers/telegramMessageContent");
const MeetingService = require("./meeting.service");

const meetingNotification = async (chat_id, message) => {
  if (!chat_id) return;

  await axios.post(`${TELEGRAM_API}/sendMessage`, {
    chat_id,
    text: message,
    parse_mode: "HTML",
  });
};

const TelegramService = {
  async getMeetingInfoForTeleNoti(bookingId) {
    try {
      const participants = await getUserListInParticipant(bookingId);
      const meetingInfo = await BookingInfo.findOne({
        where: { id: bookingId },
        include: [
          {
            model: Room,
            as: "room",
            attributes: ["roomName"],
            require: true,
            raw: true,
          },
          {
            model: Department,
            as: "department",
            attributes: ["telegramGroupId"],
            require: true,
            raw: true,
          },
        ],
        attributes: ["startingTime", "endingTime", "meetingContent", "reason"],
        raw: true,
      });

      const { startingTime, endingTime, meetingContent, reason } = meetingInfo;

      return {
        participants,
        startingTime,
        endingTime,
        meetingContent,
        roomName: meetingInfo["room.roomName"],
        chat_id: meetingInfo["department.telegramGroupId"],
        reason,
      };
    } catch (error) {
      throw error;
    }
  },

  async newMeetingNotify(bookingId) {
    try {
      const meetingInfo = await MeetingService.getMeetingInfoForTeleNoti(bookingId);
      const { chat_id } = meetingInfo;
      const message = telegramMessageContent(meetingInfo).newMeeting();
      meetingNotification(chat_id, message);
    } catch (error) {
      throw error;
    }
  },

  async cancelMeetingNotify(bookingId) {
    try {
      const meetingInfo = await MeetingService.getMeetingInfoForTeleNoti(bookingId);
      const { chat_id } = meetingInfo;
      const message = telegramMessageContent(meetingInfo).cancelMeeting();
      meetingNotification(chat_id, message);
    } catch (error) {
      throw error;
    }
  },

  async remindMeetingNotify(today) {
    try {
      const day = [`${today} 00:00:00`, `${today} 23:59:59`];
      const todayMeeting = await MeetingService.getTodayMeetings(day);
      async function notify(bookingId) {
        const meetingInfo = await MeetingService.getMeetingInfoForTeleNoti(bookingId);
        const { chat_id } = meetingInfo;
        const message = telegramMessageContent(meetingInfo).remindMeeting();
        meetingNotification(chat_id, message);
      }

      todayMeeting.forEach((meeting) => {
        notify(meeting.id);
      });
    } catch (error) {
      throw error;
    }
  },

  async updateTelegramId(chat_id, queryText) {
    try {
      const departmentName = queryText.substring(
        queryText.indexOf("(") + 1,
        queryText.lastIndexOf(")")
      );

      if (departmentName.trim() === "") return "Vui lòng đặt tên nhóm đúng định dạng.";
      const department = await Department.findOne({ where: { departmentName } });
      if (!department) return "Không tìm thấy phòng ban.";
      const update = await Department.update(
        { telegramGroupId: chat_id },
        {
          where: { id: department.id },
          raw: true,
        }
      );
      if (update) return "Kết nối telegram thành công.";
      return "Kết nối telegram thành công.";
    } catch (error) {
      throw error;
    }
  },
};

module.exports = TelegramService;
