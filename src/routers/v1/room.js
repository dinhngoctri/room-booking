const express = require("express");
const roomRouter = express.Router();
const { authorization, authorizationAdmin } = require("../../middlewares/authorization");

const getRoomList = require("../../controllers/Room/getRoomList");
roomRouter.get("/getList", getRoomList());
// roomRouter.get("/getList", authorization, getRoomList());

const getAListOfRoom = require("../../controllers/Room/getAListOfRoom");
roomRouter.get("/getAllOfRoom", getAListOfRoom());

const getRoomByID = require("../../controllers/Room/getRoomByID");
roomRouter.get("/:roomId", getRoomByID());

const putUpdateRoom = require("../../controllers/Room/updateRoom");
roomRouter.put("/updateRoom/:id", authorizationAdmin, putUpdateRoom());

const createRoom = require("../../controllers/Room/createRoom");
roomRouter.post("/createRoom", authorizationAdmin, createRoom());

const deletedRoom = require("../../controllers/Room/deletedRoom");
roomRouter.delete("/deletedRoom/:id", authorizationAdmin, deletedRoom());

const restoreRoom = require("../../controllers/Room/restoreRoom");
roomRouter.put("/restoreRoom/:id", authorizationAdmin, restoreRoom());

module.exports = roomRouter;
