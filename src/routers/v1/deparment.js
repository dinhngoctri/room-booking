const express = require("express");
const departmentRouter = express.Router();

const getDepartmentList = require("../../controllers/Department/getDepartmentList");
departmentRouter.get("/getList", getDepartmentList());

const getDepartmentByID = require("../../controllers/Department/getDepartmentByID");
departmentRouter.get("/:departmentId", getDepartmentByID());

module.exports = departmentRouter;
