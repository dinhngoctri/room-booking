const express = require("express");
const bookingInfoRouter = express.Router();
const { authorization } = require("../../middlewares/authorization");

const getBookingList = require("../../controllers/BookingInfo/getBookingList");
bookingInfoRouter.get("/getBookinginfoList", authorization, getBookingList());

const getListOfMeetingsYouAttendController = require("../../controllers/BookingInfo/getListOfMeetingsYouAttend");
bookingInfoRouter.get(
  "/getListOfMeetingYouAttend",
  getListOfMeetingsYouAttendController()
);

const deletedBookingInfo = require("../../controllers/BookingInfo/deletedBookingInfo");
bookingInfoRouter.put("/deleted/:id", deletedBookingInfo());

const sendEmailBookinInfo = require("../../controllers/BookingInfo/sendEmailBookingInfo");
bookingInfoRouter.post("/sendEmail", sendEmailBookinInfo());

const checkSendEmailSuccessController = require("../../controllers/BookingInfo/checkSendEmailSuccess");
bookingInfoRouter.put("/checkSendEmail", checkSendEmailSuccessController());

const saveEmailToParticipantsController = require("../../controllers/BookingInfo/saveEmailToParticipantsController");
bookingInfoRouter.post("/saveUserToParticipant", saveEmailToParticipantsController());

const searchBookingInfoController = require("../../controllers/BookingInfo/searchBookingInfo");
bookingInfoRouter.get("/searchBookingInfo", searchBookingInfoController());

const searchBookingInfoForAdminController = require("../../controllers/BookingInfo/searchBookingInfoForAdmin");
bookingInfoRouter.get("/searchBookinginfoAdmin", searchBookingInfoForAdminController());

const addGuestAccount = require("../../controllers/BookingInfo/addGuestAccount");
bookingInfoRouter.post('/addGuestAccount',addGuestAccount())
module.exports = bookingInfoRouter;
