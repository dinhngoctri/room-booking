const express = require("express");
const notificationRouter = express.Router();

const newMeetingNotify = require("../../controllers/Notification/newMeetingNotify");
notificationRouter.post("/newMeetingNotify", newMeetingNotify());

const cancelMeetingNotify = require("../../controllers/Notification/cancelMeetingNotify");
notificationRouter.post("/cancelMeetingNotify", cancelMeetingNotify());

module.exports = notificationRouter;
