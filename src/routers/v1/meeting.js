const express = require("express");
const { authorization } = require("../../middlewares/authorization");
const meetingRouter = express.Router();

const getBookingInfoByRoomIDAndDate = require("../../controllers/Meeting/getBookingInfoByRoomIDAndDate");
meetingRouter.post("/getBookingInfoByRoomIDAndDate", getBookingInfoByRoomIDAndDate());

const bookingRoom = require("../../controllers/Meeting/bookingRoom");
meetingRouter.post("/bookingRoom", authorization, bookingRoom());

const createRecurringMeeting = require("../../controllers/Meeting/createRecurringMeeting");
meetingRouter.post("/createRecurringMeeting", authorization, createRecurringMeeting());

const addParticipantsToMeeting = require("../../controllers/Meeting/addParticipantsToMeeting");
meetingRouter.post(
  "/addParticipantsToMeeting",
  authorization,
  addParticipantsToMeeting()
);

const recurringMeetingValidation = require("../../controllers/Meeting/recurringMeetingValidation");
meetingRouter.post(
  "/recurringMeetingValidation",
  authorization,
  recurringMeetingValidation()
);

const cancelBooking = require("../../controllers/Meeting/cancelBooking");
meetingRouter.put("/cancelBooking", authorization, cancelBooking());

const getUserListInParticipantController = require("../../controllers/Meeting/getUserListInParticipant");
meetingRouter.get(
  "/get-userlist-in-participant/:id",
  getUserListInParticipantController()
);

const listOfUsersPresentInTheMeetingController = require("../../controllers/Meeting/listOfUsersPresentInTheMeeting");
meetingRouter.get(
  "/listOfUserInTheMeeting/:id",
  listOfUsersPresentInTheMeetingController()
);

const deleteUserOutOfMeetingController = require("../../controllers/Meeting/deleteUserOutOfMeeting");
meetingRouter.put("/deleteUserOutOfMeeting/:id", deleteUserOutOfMeetingController());

const userLeavesTheMeetingRoomController = require("../../controllers/Meeting/userLeavesTheMeetingRoom");
meetingRouter.put("/userLeaveMeeting/:id", userLeavesTheMeetingRoomController());

const getDetailMeetingController = require("../../controllers/Meeting/getDetailMeeitng");
meetingRouter.get("/readMeetingDetails/:id", getDetailMeetingController());

module.exports = meetingRouter;
